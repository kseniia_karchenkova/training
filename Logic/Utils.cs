﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class Utils
    {
        public static double GetRectangleArea(double width, double height)
        {
            double area = width * height;
            return area;
        }

        public static int GetCenturyFromYear(int year)
        {
            int century = year / 100;

            return year % 100 > 0
                ? century + 1
                : century;

            /*if (year % 100 > 0)
            {                
                return century + 1;
            }
            else
            {                
                return century;
            }*/
        }

        public static bool IsPalindrome(string sentence)
        {
            for (int i = 0; i < sentence.Length / 2; i++)
            {
                int lastIndex = sentence.Length - 1 - i;

                if (sentence[i] != sentence[lastIndex])
                {
                    return false;
                }                
            }

            return true;
        }

        public static bool IsArrayOdd(int[] numbers)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                if (IsNumberEven(numbers[i]))
                {
                    return false;
                }
            }

            return true;
        }

        public static bool IsArrayEven(int[] numbers)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                if (IsNumberOdd(numbers[i]))
                {
                    return false;
                }
            }

            return true;
        }

        public static bool IsNumberEven(int number)
        {
            /*if (a % 2 == 0)
            {
                return true;
            }

            return false;*/

            return number % 2 == 0;
        }

        public static bool IsNumberOdd(int number)
        {
            //return number % 2 != 0;

            return !IsNumberEven(number);
        }

        public static int SquareOfNumber(int number)
        {
            //int square_number = number * number;
            
            return number * number;
        }

        public static Dictionary<int, int> CountFrequencyOfEachElement(int[] numbers)
        {
            Dictionary<int, int> frequency = new Dictionary<int, int>();

            foreach (int number in numbers)
            {
                if (frequency.ContainsKey(number))
                {
                    frequency[number] = ++frequency[number];
                }
                else
                {
                    frequency.Add(number, 1);
                }
            }

            return frequency;
        }

        public static void OddOrEvenArray (int[] mas)
        {
            List<int> OddArray = new List<int>();
            List<int> EvenArray = new List<int>();

            foreach (int element in mas)
            {
                if (IsNumberOdd(element))
                {
                    OddArray.Add(element);
                }
                else
                {
                    EvenArray.Add(element);
                }
            }

        }

        public static int CheckOfDublicates(int[] mas)
        {
            int duplicatesCounter = 0;
            List<int> uniqueValues = new List<int>();

            for (int i = 0; i < mas.Length; i++)
            {
                if (!uniqueValues.Contains(mas[i]))
                {
                    uniqueValues.Add(mas[i]);
                }
                else
                {
                    duplicatesCounter++;
                }
            }

            return duplicatesCounter;
        }

        public static List<int> UniqueElements(int[] mas)
        {
            List<int> uniqueValues = new List<int>();

            for (int i = 0; i < mas.Length; i++)
            {
                if (!uniqueValues.Contains(mas[i]))
                {
                    uniqueValues.Add(mas[i]);
                }
            }

            return uniqueValues;           
        }

        public static void Swap<T>(ref T firstElement, ref T secondElement)
        {
            T temp = firstElement;
            firstElement = secondElement;
            secondElement = temp;
        }

        public static void BubbleSort(int[] mas, Func<int, int, bool> comparison)
        {
            for (int i = 0; i < mas.Length; i++)
            {
                for (int j = i + 1; j < mas.Length; j++)
                {
                    if(comparison(mas[i], mas[j]))
                    {
                        Swap(ref mas[i], ref mas[j]);
                    }
                }
            }
        }

        public static bool AscendingOrder(int param1, int param2)
        {
            return param1 > param2;
        }

        public static bool DescendingOrder(int param1, int param2)
        {
            return param1 < param2;
        }

        public static Dictionary<string, int> CountFrequencyOfEachWord(string[] numbers)
        {
            Dictionary<string, int> frequency = new Dictionary<string, int>();

            foreach (string number in numbers)
            {
                if (frequency.ContainsKey(number))
                {
                    frequency[number] = ++frequency[number];
                }
                else
                {
                    frequency.Add(number, 1);
                }
            }

            return frequency;
        }

        public static string[] SplitTextIntoWords(string text)
        {
            return text.Split(new[] { '\r', '\n', '.', '!', '?', ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
        }
    }    
}
