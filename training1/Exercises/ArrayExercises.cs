﻿using Logic;
using System;
using System.Collections.Generic;

namespace training1.Exercises
{
    class ArrayExercises
    {
        public static void Text()
        {
            string text = "Rail rail services are being disrupted, while engineers in Scotland, Northern Ireland and northern England are working to restore energy supplies.A yellow warning for wind for parts of England and Wales remains in force from 18:00 BST on Thursday.";
            Console.WriteLine(text);
            string textLower = text.ToLower();

            string[] wordsArray = Utils.SplitTextIntoWords(textLower);

            var frequency = Utils.CountFrequencyOfEachWord(wordsArray);
            
            foreach (var word in frequency)
            {
                Console.WriteLine(word.Key + ": " + word.Value);
            }
        }

        public static void Array11()
        {
            int[] mas = { 10, 11, 5, 8, 12, 1, 3 };
            Console.WriteLine("Current array ");
            ConsoleUtils.PrintArray(mas);

            Utils.BubbleSort(mas, Utils.DescendingOrder);

            Console.WriteLine("Elements of array in ascending order");
            ConsoleUtils.PrintArray(mas);
        }

        public static void Array6()
        {
            System.Console.Write("Enter a positive integer(length of an array): ");
            int n = ConsoleUtils.ReadIntegerFromConsole();

            int[] mas = new int[n];

            Console.WriteLine("Enter the values of the array:");

            for (int i = 0; i < n; i++)
            {
                mas[i] = ConsoleUtils.ReadIntegerFromConsole();
            }

            Console.WriteLine("The unique elements found in the array are");
            int[] array = Utils.UniqueElements(mas).ToArray();
            ConsoleUtils.PrintArray(array);
        }

        public static void Array5()
        {
            int[] mas = { 1, 2, 3, 1, 1, 2 };
            Console.WriteLine("Total number of duplicate elements found in the array is " + Utils.CheckOfDublicates(mas));
        }

        public static void Array10()
        {
            int[] mas = { 1, 2, 3, 14, 15, 7, 4, 21 };

            List<int> oddList = new List<int>();
            List<int> evenList = new List<int>();

            foreach (int element in mas)
            {
                if (Utils.IsNumberOdd(element))
                {
                    oddList.Add(element);
                }
                else
                {
                    evenList.Add(element);
                }
            }

            int[] oddArray = oddList.ToArray();
            int[] evenArray = evenList.ToArray();

            Console.WriteLine("odd");
            ConsoleUtils.PrintArray(oddArray);

            Console.WriteLine("\neven");
            ConsoleUtils.PrintArray(evenArray);
        }

        public static void Exercise56c()
        {
            int[] numbers = { 1, 2, 2, 8, 10, 10 };

            var frequency = Utils.CountFrequencyOfEachElement(numbers);

            foreach (var element in frequency)
            {
                Console.WriteLine("this element " + element.Key + " appears " + element.Value + " times");
            }
        }

        public static void Exercise56b()
        {
            int number = 5;
            Console.WriteLine("The number is " + number);

            Console.WriteLine("The square of number = " + Utils.SquareOfNumber(number));
        }

        public static void Exercise56a()
        {
            int[] numbersOdd = { 1, 3, 5, 7, 9, 11 };
            int[] numbersEven = { 2, 4, 6, 8, 10, 12 };

            Console.WriteLine("Odd: " + Utils.IsArrayOdd(numbersOdd));
            Console.WriteLine("Even: " + Utils.IsArrayEven(numbersEven));
        }

        /*public static bool IsArrayOdd(int[] numbersOdd)
        {
            for (int i = 0; i < numbersOdd.Length; i++)
            {
                if (numbersOdd[i] % 2 == 0)
                {
                    return false;
                }
            }

            return true;
        }

        public static bool IsArrayEven(int[] numbersEven)
        {
            for (int i = 0; i < numbersEven.Length; i++)
            {
                if (numbersEven[i] % 2 != 0)
                {
                    return false;
                }
            }

            return true;
        }*/
    }
}
