﻿using Logic;
using System;
using System.Linq;

namespace training1.Exercises
{
    class BasicExercises
    {
        public static void Exercise56()
        {
            string word = "anno";
            Console.WriteLine(word);
            // Console.WriteLine(checkPalindrome(word));

            bool result = true;

            for (int i = 0; i < word.Length / 2; i++)
            {
                int lastIndex = word.Length - 1 - i;

                if (word[i] != word[lastIndex])
                {
                    result = false;
                    break;
                }
            }

            if (result)
            {
                Console.WriteLine("this word is a palindrome ");
            }
            else
            {
                Console.WriteLine("this word is not a palindrome ");
            }
        }

        /* public static bool checkPalindrome(string word)
        {
            char[] letters = word.ToCharArray();
            Array.Reverse(letters);
            return new string(letters).Equals(word);
        }*/

        public static void Exercise54()
        {
            Console.WriteLine("The year is ");
            int year = int.Parse(Console.ReadLine());

            Console.WriteLine("Century is " + Utils.GetCenturyFromYear(year));

            double width = 13.2;
            double height = 10;

            Console.WriteLine(Utils.GetRectangleArea(width, height));

            double value1 = 15 / 7;
            double value2 = 15 / 7;

            Console.WriteLine(value1 == value2);
        }

        public static void Exercise47()
        {
            int[] arrayNumbers = { 1, 2, 2, 3, 3, 4, 5, 6, 5, 7, 7, 7, 8, 8, 2 };
            int sum = 0;

            foreach (int number in arrayNumbers)
            {
                sum += number;
            }

            Console.WriteLine("the sum of all the elements of an array = " + sum);

            arrayNumbers.Reverse();

            ConsoleUtils.PrintArray(arrayNumbers);
        }

        public static void Exercise45()
        {
            int[] numbers = { 1, 2, 5, 9, 7, 5, 7 };
            Console.WriteLine("Our array ");
            for (int i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine(numbers[i]);
            }

            const int numb = 5;
            int count = 0;

            /*foreach (int number in numbers)
            {               
                if (number == numb)
                {
                    count++;                   
                }               
            }*/

            count = numbers.Where(number => number == numb).Count();

            Console.WriteLine("The number 5 occurs " + count + " times");
        }

        public static void Exercise44()
        {
            string phrase = "w3resource ";
            Console.WriteLine(phrase);

            for (int i = 0; i < phrase.Length; i += 2)
            {
                Console.WriteLine(phrase[i]);
            }
        }

        public static void Exercise42()
        {
            string phrase = "No money - NO HONEY!";
            Console.WriteLine(phrase);

            if (phrase.Length < 4)
            {
                Console.WriteLine(phrase.ToUpper());
                return;
            }

            string fourChars = phrase.Substring(0, 4);

            Console.WriteLine(fourChars.ToLower());
        }

        public static void Exercise40()
        {
            const int number = 20;

            Console.WriteLine("Input first integer:");
            int number1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Input second integer:");
            int number2 = int.Parse(Console.ReadLine());

            int subtraction1 = Math.Abs(number - number1);
            int subtraction2 = Math.Abs(number - number2);

            if (subtraction1 > subtraction2)
            {
                Console.WriteLine("The nearest number to 20 is " + number2);
            }
            else if (subtraction1 == subtraction2)
            {
                Console.WriteLine(0);
            }
            else
            {
                Console.WriteLine("The nearest number to 20 is " + number1);
            }
        }

        public static void Exercise39a()
        {
            int[] numbers = { 15, 10, 7, 16, 2, -1 };

            for (int i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine(numbers[i]);
            }

            Console.WriteLine("Highest number is " + GetHighestNumber(numbers));
            Console.WriteLine("Lowest number is " + GetLowestNumber(numbers));
        }

        public static int GetHighestNumber(int[] numbers)
        {
            int highestNumber = int.MinValue;

            foreach (int number in numbers)
            {
                if (number > highestNumber)
                {
                    highestNumber = number;
                }
            }

            return highestNumber;
        }

        public static int GetLowestNumber(int[] numbers)
        {
            int lowestNumber = int.MaxValue;

            foreach (int number in numbers)
            {
                if (number < lowestNumber)
                {
                    lowestNumber = number;
                }
            }

            return lowestNumber;
        }

        public static void Exercise39()
        {

            Console.WriteLine("Enter three numbers");

            int number1 = int.Parse(Console.ReadLine());
            int number2 = int.Parse(Console.ReadLine());
            int number3 = int.Parse(Console.ReadLine());
            //string theBiggestNumber = "The biggest number is ";

            Console.WriteLine("Largest of three: " + Math.Max(number1, Math.Max(number2, number3)));
            Console.WriteLine("Lowest of three: " + Math.Min(number1, Math.Min(number2, number3)));

            /* if (number1>number2)
            {
                if (number1 > number3)
                {
                    Console.WriteLine(theBiggestNumber + number1);
                }
                else
                {
                    Console.WriteLine(theBiggestNumber + number3);
                }               
            }

            else if (number2 > number3)
            {
                Console.WriteLine(theBiggestNumber + number2);
            }
            else
            {
                Console.WriteLine(theBiggestNumber + number3);
            }*/
        }

        public static void Exercise37()
        {
            const string testString = "HP";
            string phrase = "PHP Tutorial";

            Console.WriteLine(phrase);

            if (phrase.Substring(1, testString.Length) == testString)
            {
                string newPhrase = phrase.Remove(1, testString.Length);
                Console.WriteLine(newPhrase);
            }
        }

        public static void Exercise34()
        {
            string phrase = Console.ReadLine();
            string firstWord = "Hello";
            string[] words = phrase.Split(' ');

            Console.WriteLine("Is the first word 'Hello'?");

            bool condition = words[0] == firstWord;
            Console.WriteLine(condition);

            //if (words[0] == firstWord)
            /*if(phrase.StartsWith(firstWord))
            {
                Console.WriteLine("True");
            }
            else
            {
                Console.WriteLine("False");
            }*/
        }

        public static int GenerateRandomNumber()
        {
            var random = new Random();
            int randomNumber = random.Next();

            return randomNumber;
        }

        public static void Excercise33()
        {
            //int number = int.Parse(Console.ReadLine());
            int number = GenerateRandomNumber();

            Console.WriteLine(number);

            if (number % 3 == 0 || number % 7 == 0)
            {
                Console.WriteLine("True");
            }
            else
            {
                Console.WriteLine("False");
            }
        }

        public class A
        {
            public int i;
        }

        public static void ValueReferenceTypes()
        {
            int number = 8;
            int anotherNumber = number;
            anotherNumber = 5;

            Console.WriteLine(number);
            ChangeNumber(ref number);
            Console.WriteLine(number);

            A a = new A();
            a.i = 5;
            Console.WriteLine(a.i);
            ChangeNumber(a);
            Console.WriteLine(a.i);
        }

        public static void ChangeNumber(ref int number)
        {
            number = 2;
        }

        public static void ChangeNumber(A a)
        {
            a.i = 2;
        }

        public static void Exercise28()
        {
            const string phrase = "Display the pattern like pyramid using the alphabet 3";
            string[] wordsArray = Utils.SplitTextIntoWords(phrase);

            for (int i = 0; i < wordsArray.Length / 2; i++)//Generic Swap
            {
                /*int ElementIndex = wordsArray.Length - 1 - i;
                string element = wordsArray[i];
                wordsArray[i] = wordsArray[wordsArray.Length - 1 - i];
                wordsArray[wordsArray.Length - 1 - i] = element;*/

                Utils.Swap(ref wordsArray[i], ref wordsArray[wordsArray.Length - 1 - i]);
            }

            for (int i = 0; i < wordsArray.Length; i++)
            {
                Console.WriteLine(wordsArray[i]);
            }
        }        

        public static void Excercise27a()
        {
            var random = new Random();
            int number = random.Next(100);
            Console.WriteLine("The current number is " + number);
            int quantiyOfDigits = QuantityOfDigits(number);
            Console.WriteLine("Quantity of digits of this number is " + quantiyOfDigits);
        }

        public static int QuantityOfDigits(int number)
        {
            int numberOfDigits = 1;
            int i = 10;

            /* while (true)
             {
                 if (number % i == number)
                 {
                     break;
                 }

                 numberOfDigits++;
                 i *= 10;
             }*/

            while (number % i != number)
            {
                numberOfDigits++;
                i *= 10;
            }

            return numberOfDigits;
        }

        public static void Excercise27()
        {
            const int number = 12;
            int firstDigit = number / 10;
            int secondDigit = number % 10;
            int sum = firstDigit + secondDigit;

            Console.WriteLine(sum);
        }

        public static void Excercise25()
        {
            int i = 1;

            while (i < 100)
            {
                Console.WriteLine(i);
                i += 2;
            }
        }

        public static void Excercise24()
        {
            const string phrase = "Write a C# Sharp Program to display the:following pattern using the alphabet  ";
            string[] words = phrase.Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
            string shortestWord = phrase;

            foreach (string word in words)
            {
                if (shortestWord.Length >= word.Length)
                {
                    shortestWord = word;
                }
            }

            Console.WriteLine(shortestWord);
        }

        public static void Excercise23()
        {
            const string phrase = "PUSSY";
            string result = phrase.ToLower();
            Console.WriteLine(result);
        }

        public static void Excercise22()
        {
            const int number = 20;

            if (number >= 20 && number <= 100)
            {
                Console.WriteLine("Number is between 20 and 100");
            }
            else
            {
                Console.WriteLine("Failure");
            }
        }

        public static void Excercise19()
        {
            Console.WriteLine("Enter a number1");
            int number1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter a number2");
            int number2 = int.Parse(Console.ReadLine());

            int sum = number1 + number2;

            if (number1 == number2)
            {
                int tripleSum = sum * 3;
                Console.WriteLine(tripleSum);
            }
            else
            {
                Console.WriteLine(sum);
            }
        }

        public static void Excercise18()
        {
            Console.WriteLine("Enter a positive or negative number");
            double number = double.Parse(Console.ReadLine());

            if (number > 0)
            {
                Console.WriteLine("This number is positive");
            }
            else if (number == 0)
            {
                Console.WriteLine("This number is zero");
            }
            else
            {
                Console.WriteLine("This number is negative");
            }
        }


        public static void Excercise17()
        {
            const string phrase = "The quick brown fox jumps over the lazy dog.";

            if (phrase.Length >= 1)
            {
                // string firstLetter = phrase.Substring(0,1);
                char firstLetter = phrase.First();
                string result = firstLetter + phrase + firstLetter;
                Console.WriteLine(result);
            }
        }

        public static void Excercise10()
        {
            const string word = "Python";
            char lastSymbol = word[word.Length - 1];
            char firstSymbol = word[0];

            string result = word.Remove(0, 1);
            result = result.Remove(result.Length - 1, 1);
            result = lastSymbol + result + firstSymbol;

            Console.WriteLine(result);
        }

        public static void Excercise9()
        {
            const string testString = "w3resource";
            const string partOfString = "3r";

            int indexOfSymbol = testString.IndexOf(partOfString);
            string result = testString.Replace(partOfString, string.Empty);
            Console.WriteLine(result);
        }

        public static void Excercise8()
        {
            const int userProvidedValue = 5;

            PrintNumbers(userProvidedValue);

            for (int a = 0; a < 3; a++)
            {
                Console.WriteLine(userProvidedValue + " " + userProvidedValue);
            }

            PrintNumbers(userProvidedValue);
        }

        public static void PrintNumbers(int userProvidedValue)
        {
            for (int a = 0; a < 3; a++)
            {
                Console.Write(userProvidedValue);
            }

            Console.WriteLine();
        }

        public static int Multiply(int a, int b)
        {
            return a * b;
        }

        public static void Excercise7()
        {
            const int number1 = 3;
            const int number2 = 4;

            int result = Multiply(number1, number2);
            Console.WriteLine(result);
        }

        public static void Exercise6()
        {
            string greet = "Please enter the number ";
            Console.WriteLine(greet);
            double number = double.Parse(Console.ReadLine());

            for (int a = 0; a < 2; a++)
            {
                PrintNumbers(number);
            }
        }

        public static void PrintNumbers(double number1)
        {
            const int quantity = 4;

            for (int i = 0; i < quantity; i++)
            {
                Console.Write(number1 + " ");
            }

            Console.WriteLine();

            for (int i = 0; i < quantity; i++)
            {
                Console.Write(number1);
            }

            Console.WriteLine();
        }

        public static void Exercise5()
        {
            const int number = 5;

            string greet1 = "Please enter " + number + " numbers:";
            Console.WriteLine(greet1);


            int[] mas = new int[number];

            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = int.Parse(Console.ReadLine());
            }

            int sum = 0;
            for (int i = 0; i < mas.Length; i++)
            {
                sum = sum + mas[i];
            }

            double average = sum / number;
            Console.WriteLine(average);
        }

        public static void Exercise4()
        {
            string greet = "Please enter number:";
            Console.WriteLine(greet);
            int number = int.Parse(Console.ReadLine());

            /* for (int i = 1; i <= 10; i = i++)
             {
                 int result = number * i;
                 Console.WriteLine(number + " * " + i + " = " + result);
             }*/

            int i = 1;
            while (i <= 10)
            {
                int result = number * i;
                Console.WriteLine(number + " * " + i + " = " + result);
                i = i + 1;
            }

            Console.ReadKey();
        }

        public static void Exercise3()
        {
            string greetings1 = "Please enter number 1:";
            Console.WriteLine(greetings1);
            double number1 = double.Parse(Console.ReadLine());

            string greetings2 = "Please enter number 2:";
            Console.WriteLine(greetings2);
            double number2 = double.Parse(Console.ReadLine());

            /*double swap = number1;
            number1 = number2;
            number2 = swap;*/

            Utils.Swap(ref number1, ref number2);

            Console.WriteLine("number1=" + number1 + " number2=" + number2);

            Console.ReadKey();
        }

        public static void Exercise1()
        {
            string greetings;
            greetings = "Please enter your name:";
            Console.WriteLine(greetings);

            string name = Console.ReadLine();
            Console.WriteLine("Hello " + name);

            Console.ReadKey();
        }
    }
}
