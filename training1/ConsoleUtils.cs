﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace training1
{
    class ConsoleUtils
    {
        public static int ReadIntegerFromConsole()
        {
            return int.Parse(Console.ReadLine());
        }

        public static void PrintArray(int[] mas)
        {
            foreach (int element in mas)
            {
                Console.Write(element + " ");                
            }
            Console.WriteLine();
        }

        public static void PrintArrayOfWords(string[] mas)
        {
            foreach (string word in mas)
            {
                Console.Write(word + "\n");
            }
            Console.WriteLine();
        }
    }
}
