﻿using Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class UnitTest1
    {
        [DataRow(2, 3, 6)]
        [DataRow(5.5, 3, 16.5)]
        [DataTestMethod]
        public void GetRectangleArea_UnderTest(double width, double height, double result)
        {
            Assert.AreEqual(Utils.GetRectangleArea(width, height), result);
        }

        [DataRow(2000, 20)]
        [DataRow(1634, 17)]
        [DataTestMethod]
        public void GetCenturyFromYear_UnderTest(int year, int century)
        {
            Assert.AreEqual(Utils.GetCenturyFromYear(year), century);
        }

        [DataRow("anna", true)]
        [DataRow("abcba", true)]
        [DataRow("ab x ba", true)]
        [DataRow("ab x1 ba", false)]
        [DataTestMethod]
        public void IsPalindrome_UnderTest(string sentence, bool result)
        {
            Assert.AreEqual(Utils.IsPalindrome(sentence), result);
        }

        [DataRow(2, true)]
        [DataRow(3, false)]
        [DataTestMethod]
        public void IsNumberEven_UnderTest(int number, bool result)
        {
            Assert.AreEqual(Utils.IsNumberEven(number), result);
        }

        [TestMethod]
        public void IsArrayEven_UnderTest()
        {
            int[] numbers = { 2, 4, 6, 8, 10, 12 };

            Assert.AreEqual(Utils.IsArrayEven(numbers), true);
        }
                
        [DataRow(5, 25)]
        [DataRow(6, 36)]
        [DataTestMethod]
        public void SquareOfNumber_UnderTest(int number, int result)
        {
            Assert.AreEqual(Utils.SquareOfNumber(number), result);
        }
    }
}
